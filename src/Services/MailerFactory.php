<?php

namespace Kisphp\EmailBundle\Services;

use Kisphp\EmailBundle\Services\Mailer\AbstractMessage;
use Kisphp\EmailBundle\Services\Mailer\Type\NoMessage;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Router;

class MailerFactory
{
    const EMAIL_TYPES_MESSAGE_PATTERN = 'Kisphp\\EmailBundle\\Services\\Mailer\\Type\\%sMessage';

    /**
     * @var \Twig_Environment
     */
    protected $twig;

    /**
     * @var Router
     */
    protected $router;

    /**
     * @var AbstractMessage
     */
    protected $message;

    /**
     * @var \Swift_Mailer
     */
    protected $swiftMailer;

    /**
     * @var array
     */
    private $customData = [];

    /**
     * @var Session
     */
    private $session;

    /**
     * @var array
     */
    private $config = [];

    /**
     * @param \Twig_Environment $twig
     * @param Router $router
     * @param \Swift_Mailer $swiftMailer
     */
    public function __construct(\Twig_Environment $twig, Router $router, \Swift_Mailer $swiftMailer, Session $session, array $config)
    {
        $this->twig = $twig;
        $this->router = $router;
        $this->swiftMailer = $swiftMailer;
        $this->session = $session;
        $this->config = $config;
    }

    /**
     * @param $messageType
     *
     * @return $this
     */
    public function createMailMessage($messageType, array $customData = [])
    {
        $this->customData = $customData;
        $this->message = $this->createMessageObject($messageType);
        $this->message->createMessage($this->customData);

        return $this;
    }

    /**
     * @return AbstractMessage
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @return \Swift_Mailer
     */
    public function getSwiftMailer()
    {
        return $this->swiftMailer;
    }

    /**
     * @return bool
     */
    public function send()
    {
        if ($this->message instanceof NoMessage) {
            return 0;
        }

        return $this->swiftMailer->send($this->message->getMessage());
    }

    /**
     * @param string $messageType
     *
     * @return AbstractMessage
     */
    protected function createMessageObject($messageType)
    {
        $messageClass = sprintf(self::EMAIL_TYPES_MESSAGE_PATTERN, ucfirst($messageType));

        $className = sprintf(self::EMAIL_TYPES_MESSAGE_PATTERN, 'No');
        if (class_exists($messageClass)) {
            $className = $messageClass;
        }

        return new $className(
            $this->twig,
            $this->router,
            $this->session,
            $this->config
        );
    }
}
