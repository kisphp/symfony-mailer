<?php

namespace Kisphp\EmailBundle\Services\Mailer;

use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\Router;

abstract class AbstractMessage
{
    /**
     * @var \Swift_Message
     */
    protected $message;

    /**
     * @var \Twig_Environment
     */
    protected $twig;

    /**
     * @var Router
     */
    protected $router;

    /**
     * @var Session
     */
    protected $session;

    /**
     * @var array
     */
    protected $customData = [];

    /**
     * @var array
     */
    protected $config = [];

    /**
     * @param \Twig_Environment $twig
     * @param Router $router
     */
    public function __construct(\Twig_Environment $twig, Router $router, Session $session, array $config = [])
    {
        $this->twig = $twig;
        $this->router = $router;
        $this->session = $session;
        $this->config = $config;
    }

    /**
     * @return $this
     */
    public function createMessage(array $customData = [])
    {
        $this->customData = $customData;

        $this->message = $this->createMessageInstance();

        $this->message
            ->setSubject($this->getSubject())
            ->setFrom($this->getFromAddress(), $this->getFromName())
            ->setTo($this->getToAddress(), $this->getToName())
            ->setBody($this->getBodyHtml(), 'text/html')
        ;

        $this->setExtraParameters();

        return $this;
    }

    /**
     * @param string $email
     * @param string|null $name
     */
    public function addBcc($email, $name = null)
    {
        $this->message->addBcc($email, $name);
    }

    /**
     * @param string $email
     * @param string|null $name
     */
    public function addCc($email, $name = null)
    {
        $this->message->addCc($email, $name);
    }

    /**
     * @return \Swift_Message
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @return string
     */
    protected function getBodyHtml()
    {
        $template = $this->getMessageTemplate();

        if ($template === null) {
            return '';
        }

        return $this->twig->render(
            $template,
            $this->getMessageVariables()
        );
    }

    /**
     * @return array
     */
    protected function getMessageVariables()
    {
        return array_merge(
            $this->getGlobalVariables(),
            $this->customData,
            $this->getVariables()
        );
    }

    /**
     * @return \Swift_Message
     */
    protected function createMessageInstance()
    {
        return new \Swift_Message();
    }

    /**
     * @return array
     */
    protected function getGlobalVariables()
    {
        $rootUrl = $this->router->generate(
            $this->config['homepage_root_name'],
            [],
            UrlGeneratorInterface::ABSOLUTE_URL
        );

        return [
            'website_url' => $rootUrl,
            'website_name' => $this->config['website_name'],
        ];
    }

    /**
     * @return string
     */
    protected function getFromAddress()
    {
        return $this->config['from_email'];
    }

    /**
     * @return string
     */
    protected function getFromName()
    {
        return $this->config['from_name'];
    }

    /**
     * @return $this
     */
    protected function setExtraParameters()
    {
        //$this->message->addBcc('mail@example.com', 'alert admin');

        return $this;
    }

    /**
     * @return string
     */
    abstract protected function getMessageTemplate();

    /**
     * @return array
     */
    abstract protected function getVariables();

    /**
     * @return string
     */
    abstract protected function getSubject();

    /**
     * @return string
     */
    abstract protected function getToAddress();

    /**
     * @return string
     */
    abstract protected function getToName();
}
