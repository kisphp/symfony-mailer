<?php

namespace Kisphp\EmailBundle\Services\Mailer\Type;

use Kisphp\EmailBundle\Services\Mailer\AbstractMessage;

class RecoverPasswordMessage extends AbstractMessage
{
    /**
     * @return string
     */
    protected function getMessageTemplate()
    {
        return 'KisphpEmailBundle:Mails:recoverPassword.html.twig';
    }

    /**
     * @return array
     */
    protected function getVariables()
    {
        return [
            'reset_password_url' => $this->router->generate('user_reset_password', [
                'code' => $this->customData['password_code'],
                'hash' => $this->customData['password_hash'],
            ], true),
        ];
    }

    /**
     * @return string
     */
    protected function getSubject()
    {
        return 'Recover password';
    }

    /**
     * @return mixed
     */
    protected function getToAddress()
    {
        return $this->customData['to_email'];
    }

    /**
     * @return mixed
     */
    protected function getToName()
    {
        return $this->customData['to_name'];
    }
}
