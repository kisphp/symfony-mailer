<?php

namespace Kisphp\EmailBundle\Services\Mailer\Type;

use Kisphp\EmailBundle\Services\Mailer\AbstractMessage;

class ContactMessage extends AbstractMessage
{
    /**
     * @return string
     */
    protected function getMessageTemplate()
    {
        return 'KisphpEmailBundle:Mails:contact.html.twig';
    }

    /**
     * @return array
     */
    protected function getVariables()
    {
        return [
            'name' => $this->customData['name'],
            'email' => $this->customData['email'],
            'text' => $this->customData['text'],
        ];
    }

    /**
     * @return string
     */
    protected function getSubject()
    {
        return 'Contact ' . $this->config['website_name'];
    }

    /**
     * @return string
     */
    protected function getToAddress()
    {
        return $this->config['admin_email'];
    }

    /**
     * @return mixed
     */
    protected function getToName()
    {
        return $this->config['admin_name'];
    }
}
