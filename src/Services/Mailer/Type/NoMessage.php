<?php

namespace Kisphp\EmailBundle\Services\Mailer\Type;

use Kisphp\EmailBundle\Services\Mailer\AbstractMessage;

class NoMessage extends AbstractMessage
{
    /**
     * @return null
     */
    protected function getMessageTemplate()
    {
        return null;
    }

    /**
     * @return array
     */
    protected function getVariables()
    {
        return [];
    }

    /**
     * @return string
     */
    protected function getSubject()
    {
        return 'Do not send';
    }

    /**
     * @return string
     */
    protected function getFromAddress()
    {
        return 'no-reply@example.com';
    }

    /**
     * @return string
     */
    protected function getFromName()
    {
        return 'Do not send';
    }

    /**
     * @return string
     */
    protected function getToAddress()
    {
        return 'mail@example.com';
    }

    /**
     * @return string
     */
    protected function getToName()
    {
        return 'Do not send';
    }
}
