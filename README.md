# Symfony Mail Sender helper 


### Info

Email templates based on Foundation Email framework

### Configuration

Add to parameters.yml file:

```yaml

parameters:
    ...
    email_bundle:
        homepage_root_name: homepage # replace with your homepage route name
        website_name: Website name # replace with your website's name
        admin_email: admin@example.com # where to send contact message and alerts
        admin_name: Admin Name
        from_email: no-reply@example.com
        from_name: From my website name
```

Register new bundle in your application

```php
<?php

...

class AppKernel extends Kernel
{
    /**
     * @return array
     */
    public function registerBundles()
    {
        $bundles = array(
            ...
            new \Kisphp\EmailBundle\KisphpEmailBundle(),
        );

        ...

        return $bundles;
    }
    
    ...
}

```

> Add translations in `app/Resources/translations/messages.{lang}.yml`
