<?php

namespace Test;

use Kisphp\EmailBundle\Services\Mailer\Type\ContactMessage;

class ContactMessageTest extends \PHPUnit_Framework_TestCase
{
    public function test_a()
    {
        $twig = $this->getMock('Twig_Environment')
            ->method('render')->willReturn('asd')
        ;
        $session = $this->getMock('Symfony\Component\HttpFoundation\Session\Session');
        $route = $this->getMock('Symfony\Component\Routing\Router');

        $mes = new ContactMessage($twig, $route, $session, [
            'homepage_root_name' => 'homepage',
            'website_name' => 'Test website',
            'from_email' => 'from@example.com',
            'from_name' => 'from name',
            'admin_email' => 'admin@example.com',
            'admin_name' => 'admin website',
        ]);

        dump($mes->createMessage()->getMessage());
        die;
    }
}
